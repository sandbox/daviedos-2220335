<?php

/**
 * @file
 * Custom conditions and actions to trigger a delayed capture of funds.
 * Code basic functionality originally from commerce_authnet module.
 */

/**
 * Implements hook_rules_condition_info().
 */
function commerce_sep_capture_rules_condition_info() {
  return array(
    'commerce_sep_capture_order_moved_to_capture_status' => array(
      'label' => t('Order moved to capture status'),
      'group' => t('Commerce separate capture'),
      'parameter' => array(
        'commerce_order' => array(
          'label' => t('Order'),
          'type' => 'commerce_order',
        ),
      ),
    ),
    'commerce_sep_capture_order_paid_in_full' => array(
      'label' => t('Order is paid in full'),
      'group' => t('Commerce separate capture'),
      'parameter' => array(
        'commerce_order' => array(
          'label' => t('Order'),
          'type' => 'commerce_order',
        ),
      ),
    ),
  );
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_sep_capture_rules_action_info() {
  return array(
    'commerce_sep_capture_capture_auth_transaction' => array(
      'label' => t("Capture the amount of an order's single auth transaction"),
      'group' => t('Commerce separate capture'),
      'parameter' => array(
        'commerce_order' => array(
          'label' => t('Order'),
          'type' => 'commerce_order',
          'wrapped' => TRUE,
        ),
      ),
    ),
  );
}

/**
 * Condition callback: determine whether order is moving to the captured status.
 */
function commerce_sep_capture_order_moved_to_capture_status($order) {
  $capture_status = commerce_sep_capture_status('try_capture');
  return isset($order->original) && $order->original->status != $capture_status && $order->status == $capture_status;
}

/**
 * Condition callback: determine whether order is paid in full.
 */
function commerce_sep_capture_order_paid_in_full($order) {
  return isset($order->data['commerce_payment_order_paid_in_full_invoked']);
}

/**
 * Action hook callback.
 */
function commerce_sep_capture_capture_auth_transaction($order_wrapper) {
  $transaction = $order_wrapper->single_auth_transaction->value();
  // If a single valid auth transaction is not found, stop.
  if (!$transaction) {
    return;
  }
  $order = $order_wrapper->value();

  // Determine whether the transaction has enough authorized to capture the
  // current order balance.
  $balance = commerce_payment_order_balance($order);

  if ($balance['amount'] > $transaction->amount) {
    // Set order status if the authorization is too low.
    commerce_order_status_update($order, 'commerce_sep_capture_auth_too_low');
    return;
  }

  ctools_include('plugins');

  // See if the authorization has expired.
  $expired_callback = commerce_sep_capture_get_transaction_callback($transaction, 'expired callback');
  if ($expired_callback && $expired_callback($transaction)) {
    // Set the order status if the transaction has expired.
    commerce_order_status_update($order, 'commerce_sep_capture_auth_expired');
    // $order->status = 'commerce_sep_capture_auth_expired';
    return;
  }

  $capture_callback = commerce_sep_capture_get_transaction_callback($transaction, 'capture callback');
  if ($capture_callback) {
    // Run the capture callback for the remaining balance on the order.
    $capture_callback($transaction, $balance, $order);

    // Update the order status if the transaction was a success. We assume
    // any plugin implementers save a success status if the capture succeeded.
    // Otherwise use this modules capture-failed status.
    $status = $transaction->status == COMMERCE_PAYMENT_STATUS_SUCCESS ? commerce_sep_capture_status('captured') : 'commerce_sep_capture_failed';
    commerce_order_status_update($order, $status);
  }
}
