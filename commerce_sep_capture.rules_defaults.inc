<?php

/**
 * @file
 * Provides enabled default rules for this module.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_sep_capture_default_rules_configuration() {
  $rules = array();

  // Reaction rule: trigger capture on status change.
  $rule = rules_reaction_rule();
  $rule->active = 1;
  $rule->label = t('Capture authorized funds on order status change');

  $rule
    ->event('commerce_order_update')
    ->condition('commerce_sep_capture_order_moved_to_capture_status', array(
      'commerce_order:select' => 'commerce-order',
    ))
    ->action('component_commerce_sep_capture_capture_auth', array(
      'commerce_order:select' => 'commerce-order',
    ));

  $rules['commerce_sep_capture_update_capture_auth'] = $rule;

  // Rule component: capture funds when order is not paid in full.
  $component_parameter = array(
    'commerce_order' => array(
      'type' => 'commerce_order',
      'label' => t('Order'),
    ),
  );

  $rule = rule($component_parameter);
  $rule->active = 1;
  $rule->label = t('Capture authorized funds for an order');

  $rule
    ->condition(rules_condition('commerce_sep_capture_order_paid_in_full', array(
      'commerce_order:select' => 'commerce_order',
    ))->negate())
    ->action('commerce_sep_capture_capture_auth_transaction', array(
      'commerce_order:select' => 'commerce_order',
    ));

  $rules['commerce_sep_capture_capture_auth'] = $rule;

  return $rules;
}
