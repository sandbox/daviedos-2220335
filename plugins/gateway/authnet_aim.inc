<?php

/**
 * @file
 * CTools plugin for Authorize.net AIM.
 */

$plugin = array(
  'title' => t('Authorize.net AIM'),
  'capture callback' => 'commerce_sep_capture_authnet_aim_capture',
  'expired callback' => 'commerce_sep_capture_authnet_aim_expired',
  'type' => 'gateway',
  'auth_only status' => 'auth_only',
);

/**
 * Capture callback.
 */
function commerce_sep_capture_authnet_aim_capture($transaction, $balance) {
  // Name-value pair array for transaction.
  $nvp = array(
    'x_type' => 'PRIOR_AUTH_CAPTURE',
    'x_trans_id' => $transaction->remote_id,
    'x_amount' => commerce_currency_amount_to_decimal($balance['amount'], $balance['currency_code']),
  );

  // Submit request to Authorize.Net.
  $payment_method = commerce_payment_method_instance_load($transaction->instance_id);
  $response = commerce_authnet_aim_request($payment_method, $nvp);

  // If no approval..
  if ($response[0] != '1') {
    // Log error in watchdog and capture Authnet's response.
    watchdog('Commerce separate capture', "Prior authorization failed. !message", $variables = array('!message' => $response[3]), WATCHDOG_ALERT);
  }
  else {
    // Update capture amount to actual capture amount.
    $transaction->amount = $balance['amount'];

    // Set remote and local status accordingly.
    $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
    $transaction->remote_status = $response[11];

    // Append capture indication to result message.
    $transaction->message .= '<br />' . '<strong> ' . t('Captured:') . ' ' . '</strong>' . format_date(REQUEST_TIME, 'short');
  }
  // Save transaction based on response.
  $transaction->payload[REQUEST_TIME] = $response;
  commerce_payment_transaction_save($transaction);

  return $transaction;
}

/**
 * Expired callback.
 */
function commerce_sep_capture_authnet_aim_expired($transaction) {
  return time() - $transaction->created > 86400 * 30;
}
