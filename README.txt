CONTENTS OF THIS FILE
---------------------
* Overview
* Features include
* Limitations
* Requirements
* Configuration

OVERVIEW
--------

Commerce separate capture provides a plugin frame for capturing "prior auth" 
transactions. The module currently only contains a plugin for Authorize.net, 
but others can be written to follow a similar pattern.


FEATURES INCLUDE
----------------

Rule to trigger order transaction capture on order status update. You decide the 
target status and the success status (go to admin/commerce/config/capture).
Intelligent handling of order balance: the capture function will capture the 
amount of the order balance if it is less than or equal to the authorization.


LIMITATIONS
-----------

This module will not capture funds on orders that have more than one pending 
transaction. This choice was made to prevent partial capture of the order 
balance.


REQUIREMENTS
------------

This module is to be used with Drupal Commerce and its required modules.  The 
Commerce Authnet module is naturally required here, with more payment method 
requirements to be added upon adding to the payment gateway plugins.


CONFIGURATION
-------------

In admin/commerce/config/capture, you will need to select an order status to 
trigger prior auth capture and a payment captured status.  Any valid order 
status is available to select.
