<?php

/**
 * @file
 * Alters Commerce Order and Commerce Payment Transaction Entities.
 */

/**
 * Implements hook_entity_property_info_alter().
 */
function commerce_sep_capture_entity_property_info_alter(&$info) {
  if (isset($info['commerce_order'])) {
    $info['commerce_order']['properties']['single_auth_transaction'] = array(
      'label' => t('Authorized transaction'),
      'description' => t("The order's single authorization transaction if present"),
      'type' => 'commerce_payment_transaction',
      'getter callback' => 'commerce_sep_capture_order_properties',
    );
  }
  if (isset($info['commerce_payment_transaction'])) {
    $info['commerce_payment_transaction']['properties']['auth_only_status'] = array(
      'label' => t('Auth-only status'),
      'description' => t('The status that means the transactions has been authorized but not captured, and requires manual capture.'),
      'type' => 'text',
      'getter callback' => 'commerce_sep_capture_payment_transaction_properties',
    );
  }
}
